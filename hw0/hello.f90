program hello
  use mod_param, only: iout,&
                       pi
  implicit none
  
  write(iout,*) "Hello world"
  write(iout,*) "Easy as", pi

end program hello
