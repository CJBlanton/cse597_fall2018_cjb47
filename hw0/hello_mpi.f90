program hello_mpi
  use mod_param, only: iout,&
                       pi
  implicit none
  include 'mpif.h'  


  !MPI variables
  integer :: ierr, numtasks, rank, len
  character(MPI_MAX_PROCESSOR_NAME) :: hostname

  !MPI initialization
  call MPI_INIT(ierr)
  if (ierr .ne. 0) then
     write(iout,*) "Error initalization MPI environment!"
     STOP
  end if

  !Get number of taks
  call MPI_COMM_SIZE(MPI_COMM_WORLD,numtasks,ierr)

  !Get my rank
  call MPI_COMM_RANK(MPI_COMM_WORLD,rank,ierr)

  !Get the hostname
  call MPI_GET_PROCESSOR_NAME(hostname,len, ierr)

  
  write(iout,*) "Hello world from rank #", rank, " of ", numtasks, " on ", hostname
  write(iout,*) "Easy as", pi

  call MPI_FINALIZE(ierr)
  if (ierr .ne. 0) then
     write(iout,*) "Error finalizing MPI environment!"
     STOP
  end if


end program hello_mpi
