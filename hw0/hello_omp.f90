program hello_omp
  use mod_param, only: iout,&
                       pi
  implicit none

  integer :: myid
  integer, external :: OMP_GET_THREAD_NUM

!$OMP PARALLEL PRIVATE(myid)
  myid = OMP_GET_THREAD_NUM()
  write(iout,*) "Hello world from thread number ", myid
  write(iout,*) "Easy as", pi
!$OMP END PARALLEL 

end program hello_omp
