# Homework 0 Example in Fortran

In this section, an example of a hello world program is demostrated. An example of preparing a makefile for a Fortran program is shown.
All can be made with
make all

# Serial

This is the basic hello world in Fortran 90. To compile just the serial executable,
make serial
To run the executable
./hello.exe

# OpenMP

This is a very simple OpenMP hello world. To compile just the OpenMP executable,
make openmp
To run the OpenMP executable
OMP_NUM_THREADS=2 ./hello_omp.exe

# MPI

This is a very simple MPI hello world. To compile just the MPI executable,
make mpi

To the MPI executable
mpirun -np 2 hello_mpi.exe

# Clean
make clean removes all mod, o, and exe files. 