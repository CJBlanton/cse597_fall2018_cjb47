! Basic parameters and constants commonly used
!
! Revisions
!    Date       Programer         Comments
!    08/15/2018 C. Blanton        Original
!
module mod_param
  implicit none

  private
  public :: wp
  public :: inp
  public :: iout
  public :: maxchar
  public :: infile
  public :: ioutfile
  public :: hbar
  public :: pi
  public :: one
  public :: two
  public :: zero

!parameters
!  
  integer, parameter :: wp=8
  integer, parameter :: inp=5
  integer, parameter :: iout=6 
  integer, parameter :: maxchar=120
  integer, parameter :: infile=10
  integer, parameter :: ioutfile=11
  real(wp), parameter :: pi=(2.0e0_wp)*ACOS(0.0e0_wp)
  real(wp), parameter :: hbar=1.00e0_wp
  real(wp), parameter :: one=1.00e0_wp
  real(wp), parameter :: zero=0.00e0_wp
  real(wp), parameter :: two=2.00e0_wp

end module mod_param
